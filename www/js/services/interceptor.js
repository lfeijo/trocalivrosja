angular.module('starter.services')

// Intercepts all http requests adding a token if it exists
.factory('httpAuthInterceptor', function(Token){
	var callbackRx = /callback\((.+)\);/gm;
	return {
		request: function(config) {
			if (Token.isSet()) {
				if (!(config.headers===null||typeof config.headers === 'undefined')) {
					config.headers['Authorization'] = "Bearer " + Token.get();
				}
			} else {
				// no token set
			}
			return config;
		}
	};
});