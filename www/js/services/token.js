angular.module('starter.services')

// This is the only service that deals with storing the token
.service('Token', function($localStorage){
	return {
		isSet: function(){
			return !($localStorage.token === null || typeof $localStorage.token === 'undefined');
		},
		set: function(t){
			$localStorage.token = t;
		},
		get: function(){
			return $localStorage.token;
		},
		unset: function(){
			$localStorage.token = undefined;
		}
	};
});