angular.module('starter.services')

.factory('Transaction', function($http, server){
	return {
		new: function(offerId, info, success, error) {
			$http.post(server.url+"/transaction/"+offerId, info)
			.then(function(data){
				success(data.data);
			}, function(data) {
				error(data.data);
			});
		},
		finalize: function(transactionId, rating, success, error) {
			$http.post(server.url+"/transaction/"+transactionId+"/finalize", {eval: rating})
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		unfinished: function(success, error) {
			$http.get(server.url+"/transaction/unfinished/both")
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		finished: function(success, error) {
			$http.get(server.url+"/transaction/finished/both")
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		get: function(success, error) {
			$http.get(server.url+"/transaction/both")
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		}
	}
});