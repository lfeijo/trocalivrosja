angular.module('starter.services')

// Connects to authorization services to verify credentials and issue tokens
.service('Auth', function($http, Token, server){

	var keyStr = "ABCDEFGHIJKLMNOP" +
	               "QRSTUVWXYZabcdef" +
	               "ghijklmnopqrstuv" +
	               "wxyz0123456789+/" +
	               "=";

	var service = {
		validate: function(success, error) {
			if (Token.isSet()) {
				service.withToken(Token.get(), success, error);
			} else {
				error();
			}
			
		},
		withToken: function(token, success, error) {
			$http.get(server.url+"/auth")
			.then(function(data){
				success();
			}, function(data){
				error();
			});
			// return bool
		},
		withCredentials: function(credentials, success, error) {
			$http.post(server.url+"/auth", {
				username: credentials.username,
				password: credentials.password
			}).then(function(data){
				Token.set(data.data);
				success();
			}, function(data){
				Token.set(undefined);
				error();
			});

			// return token
		},
		logout: function() {
			Token.unset();
		},
		getDecodedToken: function() {
			var input = Token.get();
	     var output = "";
	     var chr1, chr2, chr3 = "";
	     var enc1, enc2, enc3, enc4 = "";
	     var i = 0;

	     // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
	     var base64test = /[^A-Za-z0-9\+\/\=]/g;
	     if (base64test.exec(input)) {
	     }
	     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

	     do {
	        enc1 = keyStr.indexOf(input.charAt(i++));
	        enc2 = keyStr.indexOf(input.charAt(i++));
	        enc3 = keyStr.indexOf(input.charAt(i++));
	        enc4 = keyStr.indexOf(input.charAt(i++));

	        chr1 = (enc1 << 2) | (enc2 >> 4);
	        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
	        chr3 = ((enc3 & 3) << 6) | enc4;

	        output = output + String.fromCharCode(chr1);

	        if (enc3 != 64) {
	           output = output + String.fromCharCode(chr2);
	        }
	        if (enc4 != 64) {
	           output = output + String.fromCharCode(chr3);
	        }

	        chr1 = chr2 = chr3 = "";
	        enc1 = enc2 = enc3 = enc4 = "";

	     } while (i < input.length);

	     var out = unescape(output);
	     var json = out.substring(out.indexOf("_id")+6,out.length);
	     json = json.substring(0,json.indexOf("\""));
	     return json;
	  },
	  getUserId: function() {
	  	return service.getDecodedToken();
	  }
	};
	return service;
});