angular.module('starter.services')

.factory('GoogleBooks', function($http, server){
	return {
		find: function(q, index, success, error) {
			$http.jsonp(server.googleBooks+q+"&maxResults=10&startIndex="+index+"&callback=JSON_CALLBACK")
			.success(
				function(data){
					if (typeof data.error === 'undefined' || data.error === null) {
						success(data);
					} else { // error
						error({ message: data.error.message });
					}
				}
			)
			.error(
				function(info){
					error({ message: info });
				}
			);
		},
		findIsbn: function(q, index, success, error) {
			this.find("isbn="+q+"&maxResults=10&startIndex="+index, success, error);
		},
		fromGoogle: function (item) {
			try {
				var ret = {};
				var vol = item.volumeInfo;
				if (typeof vol.imageLinks !== 'undefined' && vol.imageLinks !== null) {
					if (typeof vol.imageLinks.smallThumbnail !== 'undefined' && vol.imageLinks.smallThumbnail !== null) {
						ret.thumb = vol.imageLinks.smallThumbnail;
					} else {
						ret.thumb = vol.imageLinks[0];
					}
				} else {
					ret.thumb = "nothumb";
				}
				ret.isbn = vol.industryIdentifiers[0].identifier;
				ret.title = vol.title;
				if (typeof vol.subtitle !== 'undefined' && vol.subtitle !== null) {
					ret.title += ": "+vol.subtitle;
				}
				if (typeof vol.authors !== 'undefined' && vol.authors !== null) {
					var aut = vol.authors;
					ret.author = aut[0];
					for (var i = 1; i < aut.length; i++) {
						ret.author += "; "+ aut[i];
					};
				} else {
					ret.author = "No Author";
				}
				return ret;
			} catch (err) {
				return undefined;
			}
		}
	};
});