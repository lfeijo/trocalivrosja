angular.module('starter.services')

.factory('User', function($http, server){
	return {
		new: function(info, success, error) {
			$http.post(server.url+"/user", info)
			.then(function(data){
				success(data.data);
			}, function(data) {
				error(data.data);
			});
		},
		get: function(id, success, error) {
			$http.get(server.url+"/user/"+id)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		getUsername: function(username, success, error) {
			$http.get(server.url+"/user/username/"+username)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		update: function(user, success, error) {
			$http.put(server.url+"/user", user)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		}
	}
});