angular.module('starter.services')

.factory('Uni', function($http, server){
	return {
		getAll: function(success, error) {
			$http.get(server.url+"/uni")
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		get: function(id, success, error) {
			$http.get(server.url+"/uni/"+id)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		}
	};
});