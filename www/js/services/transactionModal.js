angular.module('starter.services')

.service('TransactionModal', function($ionicModal, $rootScope) {

	var scope = $rootScope.$new();

	this.show = function(type, offer) {
		scope.params = {
			'type': type,
			'offer': offer
		};
		var service = this;
		$ionicModal.fromTemplateUrl('templates/transaction-modal.html', {
			scope: scope,
			controller: 'TransactionModalCtrl'
		}).then(function(modal) {
			service.modal = modal;
			service.modal.show();
		});
	};

	this.hide = function() {
		this.modal.hide();
	};

});