angular.module('starter.services')

.factory('Offer', function($http, server){
	return {
		new: function(info, success, error) {
			$http.post(server.url+"/offer", info)
			.then(function(data){
				success(data.data);
			}, function(data) {
				error(data.data);
			});
		},
		get: function(id, success, error) {
			$http.get(server.url+"/offer/"+id)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		getAll: function(success, error) {
			$http.get(server.url+"/offer")
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		getByUser: function(id, success, error) {
			$http.get(server.url+"/offer/by/"+id)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		search: function(query, success, error) {
			$http.get(server.url+"/offer/search?query="+query)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		},
		update: function(data, success, error) {
			$http.put(server.url+"/offer/"+data._id, data)
			.then(function(data){
				success(data.data);
			}, function(data){
				error(data.data);
			});
		}
	}
});