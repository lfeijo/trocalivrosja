// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'angularMoment', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.constant('server', {
  'url': 'http://45.55.163.219',
  'googleBooks': 'https://www.googleapis.com/books/v1/volumes?q='
})
.constant('img', {
  'nothumb': 'http://i.imgur.com/IeG7EK2.png',
  'noavatar': 'http://i.imgur.com/rFc2nLn.png'
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.loggedout', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  .state('app.signup', {
    url: '/signup',
    views: {
      'menuContent': {
        templateUrl: 'templates/signup.html',
        controller: 'SignupCtrl'
      }
    }
  })
  .state('app.settings', {
    url: '/settings',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/settings.html',
        controller: 'SettingsCtrl'
      }
    }
  })


  .state('app.search', {
    url: '/search',
    cache: false,
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })
  .state('app.offer', {
      url: '/offer/id/:id',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/offer.html',
          controller: 'OfferCtrl'
        }
      }
    }
  )
  .state('app.addoffer', {
      url: '/offer/add',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/addOffer.html',
          controller: 'AddOfferCtrl'
        }
      }
    }
  )
  .state('app.editoffer', {
      url: '/offer/edit/:_id',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/editOffer.html',
          controller: 'EditOfferCtrl'
        }
      }
    }
  )
  .state('app.youroffers', {
      url: '/offer/your',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/yourOffers.html',
          controller: 'YourOffersCtrl'
        }
      }
    }
  )



  .state('app.transactions', {
      url: '/transactions',
      views: {
        'menuContent': {
          templateUrl: 'templates/transactions.html',
          controller: 'TransactionCtrl'
        }
      }
    }
  )

  .state('app.transactions.unfinished', {
      url: '/unfinished',
      cache: false,
      views: {
        'transactionsUnfinished': {
          templateUrl: 'templates/transactions-unfinished.html',
          controller: 'TransactionUnfinishedCtrl'
        }
      }
    }
  )
  .state('app.transactions.finished', {
      url: '/finished',
      cache: false,
      views: {
        'transactionsFinished': {
          templateUrl: 'templates/transactions-finished.html',
          controller: 'TransactionFinishedCtrl'
        }
      }
    }
  )
  .state('app.transactions.all', {
      url: '/all',
      cache: false,
      views: {
        'transactionsAll': {
          templateUrl: 'templates/transactions-all.html',
          controller: 'TransactionAllCtrl'
        }
      }
    }
  )
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/search');
  $httpProvider.interceptors.push('httpAuthInterceptor');
});
