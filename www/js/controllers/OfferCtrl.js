angular.module('starter.controllers')

.controller('OfferCtrl', function($scope, $state, $stateParams, img, Offer, Auth, TransactionModal){

	$scope.offer = {};
	$scope.state = 0; // loading

	$scope.curUser = Auth.getUserId();
	$scope.moment = moment;
	$scope.img = img;

	$scope.isUndefined = function (thing) {
    	return (typeof thing === "undefined" || thing === null);
	}

	$scope.refresh = function() {
		Offer.get($stateParams.id,
			function(offer) {
				$scope.state = 1; // loaded
				$scope.offer = offer; 
			}, function(error) {
				$scope.message = error.message;
				$scope.state = 2; // error
			}
		);
	};
	$scope.refresh();

	$scope.transaction = function(type, offer) {
		TransactionModal.show(type, $scope.offer); // gambi, for some reason getting param from ng-click failed
	};

	$scope.$on('offer-changed', function(event, args) {
		$scope.refresh();
	});


});