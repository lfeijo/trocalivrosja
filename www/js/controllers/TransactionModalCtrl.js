angular.module('starter.controllers')

.controller('TransactionModalCtrl', function($scope, $timeout, $rootScope, img, Transaction, User, Auth, TransactionModal) {
	console.log($scope.params);
	$scope.user = {
		data: {},
		state: 0,
		message: "",
		term: ""
	};

	$scope.transaction = {
		state: 0,
		message: ""
	};

	$scope.img = img;

	$scope.searchUser = function() {
		if ($scope.user.term !== '') {
			$scope.user.state = 1;
			User.getUsername($scope.user.term,
				function(user) {
					if (user._id !== Auth.getUserId()) {
						$scope.user.state = 2;
						$scope.user.data = user;
					} else {
						$scope.user.state = 3;
						$scope.user.message = "Invalid username";
					}
				},
				function(error) {
					$scope.user.state = 3;
					$scope.user.message = error.error;
				}
			);
		}
	};

	$scope.cancelUser = function() {
		$scope.user.data = {};
		$scope.user.state = 0;
		$scope.user.message = "";
		$scope.user.term = "";
	};

	$scope.doTransaction = function() {
		$scope.transaction.state = 1;
		var params = {
			'type': $scope.params.type,
			'to': $scope.user.data._id
		};
		console.log(params);
		Transaction.new(
			$scope.params.offer._id,
			params,
			function(confirm) {
				console.log(confirm);
				$scope.transaction.state = 2;
				$timeout(function(){
					$scope.close();
				}, 1500);
			},
			function(error) {
				console.log(error);
				$scope.transaction.state = 3;
				$scope.transaction.message = error.error;
			}
		);
	};

	$scope.close = function() {
		$rootScope.$broadcast('offer-changed');
		TransactionModal.hide();
	};

});