angular.module('starter.controllers')

.controller('EditOfferCtrl', function($scope, $state, $stateParams, Offer){

	$scope.offer = {
		data: {},
		loadingState: 0,
		savingState: 0,
		loadingMessage: "",
		savingMessage: ""
	};

	$scope.offer.reload = function() {
		$scope.offer.loadingState = 0;
		$scope.offer.loadingMessage = "";
		Offer.get($stateParams._id,
			function(data){
				$scope.offer.data = data;
				$scope.offer.loadingState = 1;
			},
			function(error) {
				$scope.offer.loadingMessage = "Error loading offer: "+error.error.message;
				$scope.offer.loadingState = 2;
			}
		);
	};
	$scope.offer.reload();

	$scope.offer.save = function() {
		$scope.offer.savingState = 1;
		$scope.offer.savingMessage = "";
		Offer.update($scope.offer.data,
			function(data) {
				$scope.offer.savingState = 0;
				window.history.back(); // I wish I could use ionic's history to go back but this 90s solution also works.
			},
			function(error) {
				$scope.offer.savingMessage = "Error: "+error.error;
				$scope.offer.savingState = 0;
			}
		);
	};

});