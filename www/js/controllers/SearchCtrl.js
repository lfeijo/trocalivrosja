angular.module('starter.controllers')

.controller('SearchCtrl', function($scope, $state, $ionicHistory, $timeout, $ionicScrollDelegate, img, Offer){

  $ionicHistory.clearHistory();

  $scope.allOffers = {
    data: [],
    at: 0
  };

  $scope.offers = {
    data: [],
  	state: 0,
    searchState: 0
  };

  $scope.search = {
    query: "",
    showUnavailable: false
  };

  $scope.img = img;

  $scope.loadOffers = function() {
    if ($scope.search.query==="") {
      $scope.offers.state = 0;
      Offer.getAll(
        function(offers){ // success
          $scope.offers.data = [];
          $scope.allOffers.data = offers;
          $scope.allOffers.at = 0;
          $scope.offers.state = 1;
          $scope.$broadcast('scroll.refreshComplete');
        },
        function(error){ // error
          $scope.offers.error = error;
          $scope.offers.state = 2;
          $scope.$broadcast('scroll.refreshComplete');
        }
      );
    } else {
      $scope.offers.searchState = 1;
      Offer.search($scope.search.query,
        function(offers){ // success
          $scope.offers.data = [];
          $scope.allOffers.data = offers;
          $scope.allOffers.at = 0;
          $scope.offers.state = 1;
          $scope.offers.searchState = 0;
          $scope.$broadcast('scroll.refreshComplete');
          $scope.loadMore();
        },
        function(error){ // error
          $scope.offers.error = error;
          $scope.offers.state = 2;
          $scope.$broadcast('scroll.refreshComplete');
        }
      );
    }

  };
  
  $scope.loadOffers();

  $scope.$watch('search.query', function() {
    $scope.loadOffers();
  });

  $scope.search.cancel = function() {
    $scope.search.query = "";
  };

  $scope.loadMore = function() {
    if ($scope.canLoadMore()) {
      var ammount = 5;
      var slice = $scope.allOffers.data.slice($scope.allOffers.at, $scope.allOffers.at+ammount);
      $scope.offers.data = $scope.offers.data.concat(slice);
      $scope.allOffers.at += ammount;
      $scope.$broadcast('scroll.infiniteScrollComplete');
    };
  };

  $scope.canLoadMore = function() {
    return ($scope.allOffers.at < $scope.allOffers.data.length);
  };

  // idk what this function does actually
  $scope.$on('$stateChangeSuccess', function() {
    //$scope.loadMore();
  });
  
});