angular.module('starter.controllers')

.controller('AddOfferCtrl', function($scope, $state, $ionicScrollDelegate, $ionicPopup, img, Offer, GoogleBooks){

	$scope.offerData = {};
	$scope.state = 0; // waiting for input

	$scope.search = {};
	$scope.search.results = [];
	$scope.search.total = 0;
	$scope.search.state = 0; // waiting for click
	$scope.search.idx = 0; // search results starting index
	$scope.img = img;

	function setOfferFields(data) {
		$scope.offerData.isbn = data.isbn;
		$scope.offerData.title = data.title;
		$scope.offerData.author = data.author;
		if (data.thumb !== null && typeof data.thumb !== 'undefined') {
			$scope.offerData.thumb = data.thumb;	
		} else {
			$scope.offerData.thumb = "nothumb";
		}
	};

	$scope.search.pick = function(index) {
		setOfferFields($scope.search.results[index]);
		$scope.search.results = [];
		$scope.search.message = "";
		$scope.search.state = 0; // back to available for search
		$scope.search.query = "";
		$ionicScrollDelegate.$getByHandle('mainScroll').scrollTop({
			shouldAnimate: true
		});
	};

	$scope.search.perform = function(term) {
		$scope.search.state = 1; // loading
		console.log(term);
		if (typeof term !== 'undefined' && term !== null && term !== "") {
			GoogleBooks.find(term, $scope.search.idx,
				function(res) {
					$scope.search.total = res.totalItems;
					if (res.totalItems === 0) {
						$scope.search.state = 0;
						$scope.search.message = "No matches for '"+term+"'";
					} else
					if (res.totalItems > 0) {
						$scope.search.state = 2;
						$scope.search.message = "Term '"+term+"' resulted the following, pick one:";
						$scope.search.results = [];
						for (var i = res.items.length - 1; i >= 0; i--) {
							var item = GoogleBooks.fromGoogle(res.items[i]);
							if (typeof item !== 'undefined') {
								$scope.search.results.push(item);
							};
						};
					}
				},
				function(error){
					$scope.search.state = 0;
					$scope.search.message = "Error: "+error.message;
				}
			);
		} else { // error
			$scope.search.state = 0;
			// do nothing, no string typed
		}
	};

	$scope.cancelGoogle = function() {
		$scope.search.state = 0;
		$scope.search.results = [];
		$scope.search.query = "";
		$scope.search.message = "";
		$scope.search.idx = 0;
		$ionicScrollDelegate.$getByHandle('mainScroll').scrollTop({
			shouldAnimate: true
		});
	}

	$scope.moreGoogle = function() {
		$scope.search.idx+=10;
		$scope.search.perform($scope.search.query);
		$ionicScrollDelegate.$getByHandle('mainScroll').scrollTop({
			shouldAnimate: true
		});
	}

	$scope.create = function() {
		$scope.state = 1; // loading

		if (typeof $scope.offerData.isbn !== 'undefined' && $scope.offerData.isbn !== null &&
			typeof $scope.offerData.title !== 'undefined' && $scope.offerData.title !== null &&
			typeof $scope.offerData.author !== 'undefined' && $scope.offerData.author !== null) {

			Offer.new($scope.offerData,
				function(offer) {
					$scope.state = 0; // added, redirecting
					$scope.offerData = {};
					$state.go('app.offer', { id: offer._id });
				}, function(error) {
					$scope.state = 0; // waiting for input again
					$scope.message = error.message;
				}
			);
		} else {
			$scope.message = "Fill out all fields first.";
		}

	};

	$scope.searchCamera = function () {
		try {
			cordova.plugins.barcodeScanner.scan(
			    function (result) {
			    	if (result.text!=="") {
			    		$scope.search.perform("isbn="+result.text);
			    	}
			    }, 
			    function (error) {
			    	console.log(error);
			    }
		    );
		} catch (err) {
			if (err instanceof ReferenceError) {
				$ionicPopup.alert({
					title: "Error",
					template: "Sorry, but the barcode scanner is not available on your platform yet."
				}).then(
					function(res){
						// user tapped ok
					}
				);
			} else {
				$ionicPopup.alert({
					title: "Error",
					template: "This application encountered an error trying to load the camera."
				}).then(
					function(res){
						// user tapped ok
					}
				);
			}
		}
	};

});