angular.module('starter.controllers')

.controller('SignupCtrl', function($scope, $state, $ionicHistory, Auth, Uni, User){

	//$ionicHistory.clearHistory();

	$scope.signup = {
		data: {},
		state: 0,
		message: ""
	};
	$scope.unis = {
		data: {},
		state: 0,
		message: ""
	};

	$scope.unis.fetch = function() {
		$scope.unis.state = 0;
		Uni.getAll(function(u){
			$scope.unis.data = u;
			$scope.unis.state = 1;
			$scope.unis.message = "";
		}, function(error){
			$scope.unis.data = {};
			$scope.unis.state = 2;
			$scope.unis.message = error.error;
		});
	}

	$scope.unis.fetch();

	$scope.signup.perform = function() {
		if (
			$scope.signup.data.name !== null && 		typeof $scope.signup.data.name !== 'undefined' && 		$scope.signup.data.name.trim() !== "" &&
			$scope.signup.data.username !== null && 	typeof $scope.signup.data.username !== 'undefined' && 	$scope.signup.data.username.trim() !== "" &&
			$scope.signup.data.password !== null && 	typeof $scope.signup.data.password !== 'undefined' && 	$scope.signup.data.password.trim() !== "" &&
			$scope.signup.data.phone !== null && 	typeof $scope.signup.data.phone !== 'undefined' && 		$scope.signup.data.phone.trim() !== "" &&
			$scope.signup.data.uni !== null && 		typeof $scope.signup.data.uni !== 'undefined' && 		$scope.signup.data.uni.trim() !== ""
		) {
			User.new($scope.signup.data, function(user){
				$scope.signup.state = 1;
				// if created successfully, log in right away
				Auth.withCredentials({
					username: $scope.signup.data.username,
					password: $scope.signup.data.password
				}, function(data){
					// hopefully it will authorize, redirect to search
					$ionicHistory.nextViewOptions({
						disableBack: true
					});
					$state.go('app.search');
				}, function(data){
					// if not authorized it's probably a network error
					$scope.signup.state = 2;
					$scope.signup.data = {};
					$scope.signup.message = "User was created, but something happened after that, try again later.";
				});
			}, function(error){
				$scope.signup.state = 2;
				console.log(error);
				$scope.signup.message = "Error: "+error.error.message;
			});
		} else {
			$scope.signup.state = 2;
			$scope.signup.message = "Error: All fields are required.";
		}
	};

	$scope.signup.tryAgain = function() {
		$scope.signup.state = 0;
	};

});