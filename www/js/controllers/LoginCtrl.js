angular.module('starter.controllers')

.controller('LoginCtrl', function($scope, $ionicModal, $ionicHistory, $state, Token, Auth) {

  $ionicHistory.clearHistory();
  var nextState = 'app.search';

  // Form data for the login modal
  $scope.loginData = {};
  $scope.state = 0;
  //$ionicHistory.clearHistory();

  // Retrieves token from local storage and checks if still valid
  if (Token.isSet()) {
    Auth.withToken(Token.get(),
    function(){ // success
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go(nextState);
    }, function(){ // error
      Token.unset();
      // invalid token
    });
  } else {
    // go on with credentials login
  }

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    $scope.state = 1;
    $scope.message = "";
    Auth.withCredentials($scope.loginData,
      function(){ // success
        $scope.state = 0;
        $scope.loginData = {};
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go(nextState);
      },
      function(){ // error
        $scope.loginData = {};
        $scope.state = 0;
        $scope.message = "Invalid Credentials";
      }
    );
  };

  $scope.signup = function() {
    $state.go('app.signup');
  }

});