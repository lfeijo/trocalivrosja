angular.module('starter.controllers')

.controller('YourOffersCtrl', function($scope, $state, $ionicHistory, $ionicModal, Auth, Offer, TransactionModal) {

	$scope.offers = {
		data: [],
		state: 0,
		message: ""
	}

	$scope.refresh = function() {
		Offer.getByUser(Auth.getUserId(),
			function(off) {
				$scope.offers.data = off;
				$scope.offers.state = 1;
				$scope.$broadcast('scroll.refreshComplete');
			},
			function(error) {
				$scope.offers.state = 2;
				$scope.offers.message = error.message;
				$scope.$broadcast('scroll.refreshComplete');
			}
		);
	};
	$scope.refresh();


	$scope.transaction = function(type, offer) {
		TransactionModal.show(type, offer);
	};



});