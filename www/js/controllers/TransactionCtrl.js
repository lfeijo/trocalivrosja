angular.module('starter.controllers')

.controller('TransactionCtrl', function($scope, $state, $ionicHistory) {

	$scope.tab = function(name) {
        $ionicHistory.nextViewOptions({
        	disableAnimate: true,
			disableBack: true
        });
		$state.go(name);
	};

	$scope.moment = moment;

	$scope.getDuration = function(t) {
		return moment.duration(((new Date(t.ended))-(new Date(t.when)))/1000, "seconds").humanize();
	};

})

.controller('TransactionUnfinishedCtrl', function($scope, $ionicPopup, Transaction, Auth) {
	$scope.transactions = {
		data: [],
		state: 0,
		message: ""
	};

	$scope.curUser = Auth.getUserId();

	$scope.refresh = function() {
		Transaction.unfinished(
			function(t){
				$scope.transactions.data = t;
				$scope.transactions.state = 1;
				$scope.$broadcast('scroll.refreshComplete');
			},
			function(error){
				$scope.transactions.state = 2;
				$scope.transactions.message = error.message;
				$scope.$broadcast('scroll.refreshComplete');
			}
		);
	};
	$scope.refresh();

	$scope.clickFinalize = function(id) {
		var confirmPopup = $ionicPopup.confirm(
			{
				title: 'Finalize Transaction',
				template: 'Are you sure you want to complete this transaction? \
				\nTip: only finalize transactions when you have already gotten the book back.'
			}
		);
		confirmPopup.then(
			function(res) {
				if(res) {
					rateTransaction(id);
				}
			}
		);
	};

	var rateTransaction = function(id) {
		var confirmPopup = $ionicPopup.show(
			{
				title: 'Rate Transaction',
				template: 'How would you rate this transaction?',
				buttons: [
					{
						type: "icon ion-thumbsup button-balanced",
						onTap: function(){finalize(id, 1);}
					},
					{
						type: "icon ion-thumbsdown button-assertive",
						onTap: function(){finalize(id, -1);}
					}
				]
			}
		);
	};

	var finalize = function(id, rating) {
		Transaction.finalize(id, rating,
			function(confirm) {
				for (var i = $scope.transactions.data.length - 1; i >= 0; i--) {
					if ($scope.transactions.data[i]._id === id) {
						$scope.transactions.data.splice(i);
						return;
					}
				};
			},
			function(error) {
				var alertPopup = $ionicPopup.alert(
					{
						title: 'Error!',
						template: 'Something went wrong, try again later.'
					}
				);
				alertPopup.then(
					function(res) {
						// nothing
					}
				);
			}
		);
	};

})

.controller('TransactionFinishedCtrl', function($scope, Transaction) {
	$scope.transactions = {
		data: [],
		state: 0,
		message: ""
	};

	$scope.refresh = function() {
		Transaction.finished(
			function(t){
				$scope.transactions.data = t;
				$scope.transactions.state = 1;
				$scope.$broadcast('scroll.refreshComplete');
			},
			function(error){
				$scope.transactions.state = 2;
				$scope.transactions.message = error.message;
				$scope.$broadcast('scroll.refreshComplete');
			}
		);
	};
	$scope.refresh();

})

.controller('TransactionAllCtrl', function($scope, Transaction) {
	$scope.transactions = {
		data: [],
		state: 0,
		message: ""
	};

	$scope.refresh = function() {
		Transaction.get(
			function(t){
				$scope.transactions.data = t;
				$scope.transactions.state = 1;
				$scope.$broadcast('scroll.refreshComplete');
			},
			function(error){
				$scope.transactions.state = 2;
				$scope.transactions.message = error.message;
				$scope.$broadcast('scroll.refreshComplete');
			}
		);
	};
	$scope.refresh();

})

;