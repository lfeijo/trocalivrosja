angular.module('starter.controllers')

.controller('AppCtrl', function($scope, $ionicModal, $ionicHistory, $ionicPopup, $state, Auth) {

  Auth.validate(
    function() {// success
    }, function() {// error
      $ionicHistory.nextViewOptions({
        disableBack: true
      });
      $state.go('app.loggedout');
    }
  );

  $scope.showConfirmLogout = function() {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Log out',
     template: 'Are you sure you want to log out?'
   });
   confirmPopup.then(function(res) {
     if(res) {
       logout();
     } else {
     }
   });
 };

  var logout = function(){
    Auth.logout();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();
    $ionicHistory.nextViewOptions({
      disableBack: true
    });
    $state.go('app.loggedout');
  };

});