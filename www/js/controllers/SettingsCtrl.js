angular.module('starter.controllers')

.controller('SettingsCtrl', function($scope, $state, $ionicPopup, User, Auth){
	
	$scope.settings = {
		data: {},
		loadingState: 0,
		savingState: 0,
		loadingMessage: "",
		savingMessage: ""
	};

	$scope.settings.reloadUser = function() {
		$scope.settings.loadingState = 0;
		$scope.settings.loadingMessage = "";
		User.get(Auth.getUserId(),
			function(data){
				$scope.settings.data = data;
				$scope.settings.loadingState = 1;
			},
			function(error) {
				$scope.settings.loadingMessage = "Error loading settings: "+error;
				$scope.settings.loadingState = 2;
			}
		);
	};
	$scope.settings.reloadUser();

	$scope.settings.save = function() {
		$scope.settings.savingState = 1;
		$scope.settings.savingMessage = "";
		User.update($scope.settings.data,
			function(data) {
				$scope.settings.savingState = 0;
			},
			function(error) {
				$scope.settings.savingMessage = "Error: "+error;
				$scope.settings.savingState = 0;
			}
		);
	};

	$scope.getGrade = function() {
		if ($scope.settings.data.karma!==0 && $scope.settings.data.transaction_c!==0) {
			return Number((($scope.settings.data.karma/$scope.settings.data.transaction_c)*100).toFixed(0))+"%";
		} else {
			return "-";
		}
	};

});